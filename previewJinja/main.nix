{
  inputs,
  makePythonEnvironment,
  makeScript,
  projectPath,
  ...
}: let
  pythonEnvironment = makePythonEnvironment {
    pythonProjectDir = projectPath "/previewJinja";
    pythonVersion = "3.9";
  };
in
  makeScript {
    entrypoint = ./entrypoint.sh;
    name = "previewJinja";
    replace = {
      __argSrc__ = projectPath "/previewJinja/src";
    };
    searchPaths = {
      bin = [inputs.nixpkgs.python39];
      source = [pythonEnvironment];
    };
  }
