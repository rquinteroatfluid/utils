function main {
  local output_path="${STATE}"
  local template_path="${1}"
  local template_context="${2:-"{}"}"

  echo "[INFO] Previewing jinja template ${template_path}" \
    && python3 "__argSrc__/main.py" \
      "${output_path}" \
      "${template_path}" \
      "${template_context}" \
    || return 1
}

main "${@}"
