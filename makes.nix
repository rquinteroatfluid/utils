{fetchNixpkgs, ...}: {
  extendingMakesDirs = ["/"];
  inputs = {
    nixpkgs = fetchNixpkgs {
      rev = "5f5210aa20e343b7e35f40c033000db0ef80d7b9";
      sha256 = "0yc83iqbzj1gd651x6p2wx7a76wfpifaq7jxz4srbinaglfwfb07";
    };
  };
}
